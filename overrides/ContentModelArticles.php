<?php
/**
 * @package n3t Workflow
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2017-2020 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

use Joomla\Registry\Registry;

class ContentModelArticles extends n3tWorkflowContentModelArticles
{
	public function __construct($config = array())
	{
		parent::__construct($config);

		if (!empty($this->filter_fields)) {
    	$this->filter_fields[] = 'n3tworkflow_state';
      $this->filter_fields[] = 'n3tworkflow_creator';
      $this->filter_fields[] = 'n3tworkflow_editor';
      $this->filter_fields[] = 'n3tworkflow_corrector';
      $this->filter_fields[] = 'n3tworkflow_publisher';
			if (n3tWorkflowHelper::params('filter_date_created'))
      	$this->filter_fields[] = 'n3tworkflow_created';
      if (n3tWorkflowHelper::params('filter_date_published'))
      	$this->filter_fields[] = 'n3tworkflow_published';
		}
	}

	protected function getListQuery()
	{
  	$db = $this->getDbo();

    if (n3tWorkflowHelper::params('list_date_published')) {
	    $orderCol = $this->state->get('list.ordering', 'a.id');
			if ($orderCol == 'created' || $orderCol == 'a.created')
	    	$this->state->set('list.ordering', 'a.publish_up');
		}

  	$query = parent::getListQuery()
			->select('n3t.state as n3tworkflow_state')
      ->select('n3t.creator as n3tworkflow_creator')
      ->select('n3t.editor as n3tworkflow_editor')
      ->select('n3t.corrector as n3tworkflow_corrector')
      ->select('n3t.publisher as n3tworkflow_publisher')
			->join('LEFT', $db->quoteName('#__n3tworkflow_content') . ' AS n3t ON n3t.article_id = a.id');

    if (n3tWorkflowHelper::params('list_date_published'))
			$this->state->set('list.ordering', $orderCol);

		$state = $this->getState('filter.n3tworkflow.state');
  	if (is_numeric($state))
			$query->where('n3t.state = ' . $state);

		$userId = $this->getState('filter.n3tworkflow.creator');
  	if (is_numeric($userId))
			$query->where('n3t.creator = ' . $userId);

		$userId = $this->getState('filter.n3tworkflow.editor');
  	if (is_numeric($userId))
			$query->where('n3t.editor = ' . $userId);

		$userId = $this->getState('filter.n3tworkflow.corrector');
  	if (is_numeric($userId))
			$query->where('n3t.corrector = ' . $userId);

		$userId = $this->getState('filter.n3tworkflow.publisher');
  	if (is_numeric($userId))
			$query->where('n3t.publisher = ' . $userId);

    if (n3tWorkflowHelper::params('filter_date_created')) {
			$date = $this->getState('filter.n3tworkflow.created');
			if (!empty($date))
	   		$query->where('DATE(a.created) = ' . $db->quote($this->getState('filter.n3tworkflow.created')));
		}

    if (n3tWorkflowHelper::params('filter_date_published')) {
			$date = $this->getState('filter.n3tworkflow.published');
			if (!empty($date))
	   		$query->where('DATE(a.publish_up) = ' . $db->quote($this->getState('filter.n3tworkflow.published')));
		}

    return $query;
	}

	public function getItems()
	{
		$items = parent::getItems();

		foreach ($items as $item) {
    	$item->isFeatured = $item->featured;
			$item->featured = $item;
      if (n3tWorkflowHelper::params('list_date_published'))
      	$item->created = $item->publish_up;
		}

		return $items;
	}

  public function getFilterForm($data = array(), $loadData = true)
 	{
		$form = parent::getFilterForm($data, false);

		if ($form instanceof JForm) {
    	JForm::addFormPath(__DIR__ . '/../forms');
      JForm::addFieldPath(__DIR__ . '/../fields');
			$form->loadFile('filter_n3tworkflow', $loadData);
		}

    $form = parent::getFilterForm($data, $loadData);

 		if (!n3tWorkflowHelper::params('filter_date_created'))
      $form->removeField('n3tworkflow_created', 'filter');

 		if (!n3tWorkflowHelper::params('filter_date_published'))
      $form->removeField('n3tworkflow_published', 'filter');

		return $form;
	}

	protected function getStoreId($id = '')
	{
		$id = parent::getStoreId($id);

    $id .= ':' . $this->getState('filter.n3tworkflow.state');
    $id .= ':' . $this->getState('filter.n3tworkflow.creator');
    $id .= ':' . $this->getState('filter.n3tworkflow.editor');
    $id .= ':' . $this->getState('filter.n3tworkflow.corrector');
    $id .= ':' . $this->getState('filter.n3tworkflow.publisher');
    if (n3tWorkflowHelper::params('filter_date_created'))
    	$id .= ':' . $this->getState('filter.n3tworkflow.created');
    if (n3tWorkflowHelper::params('filter_date_published'))
    	$id .= ':' . $this->getState('filter.n3tworkflow.published');

		return $id;
	}

  protected function populateState($ordering = 'a.id', $direction = 'desc')
	{
    parent::populateState($ordering, $direction);

		$state = $this->getUserStateFromRequest($this->context . '.filter.n3tworkflow.state', 'filter_n3tworkflow_state');
		$this->setState('filter.n3tworkflow.state', $state);

		$userId = $this->getUserStateFromRequest($this->context . '.filter.n3tworkflow.creator', 'filter_n3tworkflow_creator');
		$this->setState('filter.n3tworkflow.creator', $userId);

		$userId = $this->getUserStateFromRequest($this->context . '.filter.n3tworkflow.editor', 'filter_n3tworkflow_editor');
		$this->setState('filter.n3tworkflow.editor', $userId);

		$userId = $this->getUserStateFromRequest($this->context . '.filter.n3tworkflow.corrector', 'filter_n3tworkflow_corrector');
		$this->setState('filter.n3tworkflow.corrector', $userId);

		$userId = $this->getUserStateFromRequest($this->context . '.filter.n3tworkflow.publisher', 'filter_n3tworkflow_publisher');
		$this->setState('filter.n3tworkflow.publisher', $userId);

    if (n3tWorkflowHelper::params('filter_date_created')) {
			$date = $this->getUserStateFromRequest($this->context . '.filter.n3tworkflow.created', 'filter_n3tworkflow_created', '');
	    try {
	    	new JDate($date);
				$this->setState('filter.n3tworkflow.created', $date);
	    } catch (Exception $e) {
				$this->setState('filter.n3tworkflow.created', null);
			}
		}

    if (n3tWorkflowHelper::params('filter_date_published')) {
			$date = $this->getUserStateFromRequest($this->context . '.filter.n3tworkflow.published', 'filter_n3tworkflow_published', '');
	    try {
	    	new JDate($date);
				$this->setState('filter.n3tworkflow.published', $date);
	    } catch (Exception $e) {
				$this->setState('filter.n3tworkflow.published', null);
			}
		}
	}
}
