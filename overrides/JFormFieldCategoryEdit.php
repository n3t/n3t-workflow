<?php
/**
 * @package n3t Workflow
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2017-2020 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

class JFormFieldCategoryEdit extends n3tWorkflowJFormFieldCategoryEdit
{
 	protected function getOptions()
	{
    $jinput = JFactory::getApplication()->input;
    $name = (string) $this->element['name'];

    $oldCat = $this->form->getValue($name, 0);
    if (
			!$this->element['parent']
			&& $jinput->get('option') != 'com_categories'
      && !$this->form->getValue('state', 0)
		)
      $this->form->setValue($name, 0);

		$options = parent::getOptions();

    $this->form->setValue($name, $oldCat);

		return $options;
	}
}
