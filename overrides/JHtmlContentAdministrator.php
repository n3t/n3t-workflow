<?php
/**
 * @package n3t Workflow
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2017-2020 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

abstract class JHtmlContentAdministrator extends n3tWorkflowJHtmlContentAdministrator
{
	public static function featured($item, $i, $canChange = true)
	{
    $html = parent::featured($item->isFeatured, $i, $canChange);

		$html .= JHtml::_('n3tworkflow.liststate', $item);

		return $html;
	}
}
