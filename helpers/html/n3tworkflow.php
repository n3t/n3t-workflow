<?php
/**
 * @package n3t Workflow
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2017-2020 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

class JHtmlN3tWorkflow {

	public static function listState($item) {
		switch ($item->n3tworkflow_state) {
			case n3tWorkFlowHelper::STATE_DRAFT:
			default:
				$title = JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_WORKFLOW_STATE_NONE');
        $btn = '';
				$icon = 'pencil-2';
				break;
			case n3tWorkFlowHelper::STATE_AUTHOR_REJECTED:
				$title = JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_WORKFLOW_STATE_AUTHOR_REJECTED');
        $btn = 'btn-danger';
				$icon = 'pencil-2';
				break;
			case n3tWorkFlowHelper::STATE_EDITOR:
				$title = JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_WORKFLOW_STATE_EDITOR');
        $btn = 'btn-info';
				$icon = 'eye';
				break;
			case n3tWorkFlowHelper::STATE_EDITOR_REJECTED:
				$title = JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_WORKFLOW_STATE_EDITOR_REJECTED');
        $btn = 'btn-primary';
				$icon = 'eye';
				break;
			case n3tWorkFlowHelper::STATE_CORRECTOR:
				$title = JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_WORKFLOW_STATE_CORRECTOR');
        $btn = 'btn-warning';
				$icon = 'checkin';
				break;
			case n3tWorkFlowHelper::STATE_CORRECTOR_REJECTED:
				$title = JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_WORKFLOW_STATE_CORRECTOR_REJECTED');
        $btn = 'btn-warning';
				$icon = 'checkin';
				break;
			case n3tWorkFlowHelper::STATE_PUBLISHER:
				$title = JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_WORKFLOW_STATE_PUBLISHER');
        $btn = 'btn-success';
				$icon = 'checkmark-circle';
				break;
			case n3tWorkFlowHelper::STATE_LOCKED:
				$title = JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_WORKFLOW_STATE_LOCKED');
        $btn = 'btn-inverse';
				$icon = 'lock';
				break;
		}

    $html = '<span class="btn btn-micro ' . $btn . ' hasTooltip" title="'
				. JHtml::tooltipText($title) . '"><span class="icon-' . $icon . '"></span></span>';
		return $html;
	}
}