<?php
/**
 * @package n3t Workflow
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2017-2020 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

use Joomla\Registry\Registry;

class n3tWorkflowHelper {

  const STATE_DRAFT = 0;
  const STATE_AUTHOR_REJECTED = 1;
  const STATE_EDITOR = 10;
  const STATE_EDITOR_REJECTED = 11;
  const STATE_CORRECTOR = 20;
  const STATE_CORRECTOR_REJECTED = 21;
  const STATE_PUBLISHER = 30;
  const STATE_LOCKED = 100;

  public static function articleWorkflow($article)
  {
    if (is_object($article))
      $id = $article->id;
    else
      $id = $article;

    $db = JFactory::getDbo();
    $query = $db->getQuery(true)
      ->select('*')
      ->from('#__n3tworkflow_content')
      ->where($db->quoteName('article_id') . ' = ' . $id);
    $db->setQuery($query);

    return $db->loadObject();
  }

  public static function params($key = null)
  {
    static $params = null;

    if (!$params) {
      $params = new Registry(JPluginHelper::getPlugin('system', 'n3tworkflow')->params);
      $params->def('filter_date_created', 1);
      $params->def('filter_date_published', 1);
      $params->def('list_date_published', 0);
    }

    return $key ? $params->get($key) : $params;
  }

  private static function checkUserType($group)
  {
    static $results = array();

    if (!isset($results[$group])) {
      $results[$group] = true;

      if ($groups = self::params($group . '_usergroup')) {
        $user = JFactory::getUser();
        $userGroups = $user->getAuthorisedGroups();

        JArrayHelper::toInteger($groups);
        $intersect = array_intersect($userGroups, $groups);
        $results[$group] = !empty($intersect);
      }
    }

    return $results[$group];
  }

  public static function isEditor()
  {
    return self::checkUserType('editor');
  }

  public static function isCorrector()
  {
    return self::checkUserType('corrector');
  }

  public static function isPublisher()
  {
    return self::checkUserType('publisher');
  }

  public static function isAdmin()
  {
    return self::checkUserType('admin');
  }

  private static function getUsersType($group)
  {
    static $results = array();

    if (!isset($results[$group])) {
      $results[$group] = array();

      if ($groups = self::params($group . '_usergroup')) {
        JArrayHelper::toInteger($groups);

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select('u.id AS value, u.name AS text')
          ->from('#__users AS u')
          ->join('INNER', '#__user_usergroup_map AS uum ON uum.user_id = u.id')
          ->where('uum.group_id in (' . implode(', ', $groups) . ')')
          ->group('u.id, u.name')
          ->order('u.name');

        $db->setQuery($query);

        $results[$group] = $db->loadObjectList();
      }
    }

    return $results[$group];
  }

  public static function getCreatorsList()
  {
    static $results = null;

    if ($results === null) {
      $db = JFactory::getDbo();
      $query = $db->getQuery(true);

      $query->select('u.id AS value, u.name AS text')
        ->from('#__users AS u')
        ->join('INNER', '#__n3tworkflow_content AS c ON c.creator = u.id')
        ->group('u.id, u.name')
        ->order('u.name');

      $db->setQuery($query);

      $results = $db->loadObjectList();
    }

    return $results;
  }

  public static function getEditorsList()
  {
    return self::getUsersType('editor');
  }

  public static function getCorrectorsList()
  {
    return self::getUsersType('corrector');
  }

  public static function getPublishersList()
  {
    return self::getUsersType('publisher');
  }

}