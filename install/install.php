<?php
/**
 * @package n3t Workflow
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2017-2020 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

use Joomla\Registry\Registry;

class plgSystemN3tWorkflowInstallerScript
{
	var $oldVersion = null;
  var $newVersion = null;

  public function preflight($type, $parent)
  {
		if ($type == 'update') {
	 		$db = JFactory::getDbo();
  	  $query = $db->getQuery(true)
				->select($db->quoteName('manifest_cache'))
      	->from($db->quoteName('#__extensions'))
      	->where($db->quoteName('name') . ' = ' . $db->quote('plg_system_n3tworkflow'));
	 		$db->setQuery($query);
			$manifest = $db->loadResult();

			$manifest = new Registry($db->loadResult());
			$this->oldVersion = $manifest->get('version');
		}
  }

	public function update($parent)
	{
  	$this->newVersion = $parent->get('manifest')->version;
    JFactory::getApplication()->enqueueMessage(JText::sprintf('PLG_SYSTEM_N3TWORKFLOW_INSTALL_UPDATE_VERSION', $this->oldVersion, $this->newVersion));
  	$this->checkUpdateServers($parent);
    $this->updateData($parent);
	}

  protected function checkUpdateServers($parent)
	{		
    $eid = $parent->get('currentExtensionId');    		
		$manifest = $parent->getManifest();
    
		$updateservers	= $manifest->updateservers;
		if ($updateservers) 
      $updateservers = $updateservers->children();
		else
			$updateservers = array();
      
    $locations = array();
		foreach ($updateservers as $updateserver)
      $locations[] = trim($updateserver); 
      
    $db = JFactory::getDbo();
    
    $query = $db->getQuery(true)
      ->select($db->quoteName(array('a.update_site_id', 'a.location')))      
      ->from($db->quoteName('#__update_sites', 'a'))
      ->join('INNER', $db->quoteName('#__update_sites_extensions', 'b') . ' ON (' . $db->quoteName('a.update_site_id') . ' = ' . $db->quoteName('b.update_site_id') . ')')
      ->group($db->quoteName(array('a.update_site_id', 'a.location')))            
      ->where($db->quoteName('b.extension_id') . '=' . $eid);
    $current_sites = $db->setQuery($query)->loadObjectList();

    $delete_sites_extensions = array();
    $delete_sites = array();
    foreach($current_sites as $current_site) {
      if (!in_array($current_site->location, $locations)) {
        $delete_sites_extensions[] = $current_site->update_site_id;
        $query->clear()
          ->select('count('.$db->quoteName('extension_id').')')        
          ->from($db->quoteName('#__update_sites_extensions'))
          ->where($db->quoteName('update_site_id') . '=' . $current_site->update_site_id)
          ->where($db->quoteName('extension_id') . '!=' . $eid);
        $current_site->extensions_count = $db->setQuery($query)->loadResult();
         
        if ($current_site->extensions_count == 0)
          $delete_sites[] = $current_site->update_site_id;
      }
    }    
    
    if (count($delete_sites_extensions)) {
      $query->clear()
        ->delete($db->quoteName('#__update_sites_extensions'))
        ->where(array(
          $db->quoteName('extension_id') . '=' . $eid,
          $db->quoteName('update_site_id') . ' in (' . implode(',', $delete_sites_extensions) . ')'          
        ));        
      $db->setQuery($query)->execute();
    }  
    
    if (count($delete_sites)) {
      $query->clear()
        ->delete($db->quoteName('#__update_sites'))
        ->where($db->quoteName('update_site_id') . ' in (' . implode(',', $delete_sites) . ')');
      $db->setQuery($query)->execute();
    }
	}

  protected function updateData($parent)
	{
		if (version_compare($this->oldVersion, '3.1.0', '<')) {
			require_once(JPATH_SITE . '/plugins/system/n3tworkflow/helpers/n3tworkflow.php');

 	 		$db = JFactory::getDbo();
  	  $query = $db->getQuery(true)
				->select($db->quoteName(array('id', 'attribs', 'created_by')))
      	->from($db->quoteName('#__content'));
	 		$db->setQuery($query);
			$articles = $db->loadObjectList();

			foreach($articles as $article) {
				$attribs = new Registry($article->attribs);

				$workflow = new stdClass();
				$workflow->article_id = $article->id;
        $workflow->creator = $article->created_by;

				switch ($attribs->get('n3tworkflowstate')) {
					default: $workflow->state = n3tWorkflowHelper::STATE_DRAFT; break;
					case 'author_rejected': $workflow->state = n3tWorkflowHelper::STATE_AUTHOR_REJECTED; break;
					case 'editor': $workflow->state = n3tWorkflowHelper::STATE_EDITOR; break;
					case 'editor_rejected': $workflow->state = n3tWorkflowHelper::STATE_EDITOR_REJECTED; break;
					case 'corrector': $workflow->state = n3tWorkflowHelper::STATE_CORRECTOR; break;
					case 'corrector_rejected': $workflow->state = n3tWorkflowHelper::STATE_CORRECTOR_REJECTED; break;
					case 'publisher': $workflow->state = n3tWorkflowHelper::STATE_PUBLISHER; break;
					case 'locked': $workflow->state = n3tWorkflowHelper::STATE_LOCKED; break;
				}

        $workflow->notes = $attribs->get('n3tworkflownotes');

				$db->insertObject('#__n3tworkflow_content', $workflow);
			}

      JFactory::getApplication()->enqueueMessage(JText::_('PLG_SYSTEM_N3TWORKFLOW_INSTALL_DATA_UPDATED'));
		}
	}

}