/**
 * @package n3t Workflow
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2017-2020 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

CREATE TABLE IF NOT EXISTS `#__n3tworkflow_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL DEFAULT 0,
  `state` int(11) NOT NULL DEFAULT 0,
  `creator` int(11) NOT NULL DEFAULT 0,
  `editor` int(11) NOT NULL DEFAULT 0,
  `corrector` int(11) NOT NULL DEFAULT 0,
  `publisher` int(11) NOT NULL DEFAULT 0,
  `notes` longtext null,
  PRIMARY KEY (`id`),
  UNIQUE `uq_article_id` (`article_id`)
) DEFAULT CHARSET=utf8;