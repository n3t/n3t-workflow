<?php
/**
 * @package n3t Workflow
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2017-2020 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

use Joomla\Registry\Registry;

class ContentModelUpcoming extends JModelList
{

	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.id',
				'title', 'a.title',
				'alias', 'a.alias',
				'checked_out', 'a.checked_out',
				'checked_out_time', 'a.checked_out_time',
				'catid', 'a.catid', 'category_title',
				'state', 'a.state',
				'created', 'a.created',
				'modified', 'a.modified',
				'created_by', 'a.created_by',
				'created_by_alias', 'a.created_by_alias',
				'ordering', 'a.ordering',
				'featured', 'a.featured',
				'publish_up', 'a.publish_up',
				'publish_down', 'a.publish_down',
				'author_id',
				'category_id',
				'level',
				'tag',
        'n3tworkflow_state', 'n3tworkflow_created', 'n3tworkflow_published',
        'n3tworkflow_creator',
				'n3tworkflow_editor',
				'n3tworkflow_corrector',
				'n3tworkflow_publisher',
			);
		}

    JForm::addFormPath(__DIR__ . '/forms');
		JForm::addFieldPath(__DIR__ . '/fields');

		parent::__construct($config);
	}

	protected function populateState($ordering = null, $direction = null)
	{
		$app = JFactory::getApplication();

		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$authorId = $app->getUserStateFromRequest($this->context . '.filter.author_id', 'filter_author_id');
		$this->setState('filter.author_id', $authorId);

		$categoryId = $this->getUserStateFromRequest($this->context . '.filter.category_id', 'filter_category_id');
		$this->setState('filter.category_id', $categoryId);

		$level = $this->getUserStateFromRequest($this->context . '.filter.level', 'filter_level');
		$this->setState('filter.level', $level);

		$tag = $this->getUserStateFromRequest($this->context . '.filter.tag', 'filter_tag', '');
		$this->setState('filter.tag', $tag);

		$state = $this->getUserStateFromRequest($this->context . '.filter.n3tworkflow.state', 'filter_n3tworkflow_state');
		$this->setState('filter.n3tworkflow.state', $state);

		$userId = $this->getUserStateFromRequest($this->context . '.filter.n3tworkflow.creator', 'filter_n3tworkflow_creator');
		$this->setState('filter.n3tworkflow.creator', $userId);

		$userId = $this->getUserStateFromRequest($this->context . '.filter.n3tworkflow.editor', 'filter_n3tworkflow_editor');
		$this->setState('filter.n3tworkflow.editor', $userId);

		$userId = $this->getUserStateFromRequest($this->context . '.filter.n3tworkflow.corrector', 'filter_n3tworkflow_corrector');
		$this->setState('filter.n3tworkflow.corrector', $userId);

		$userId = $this->getUserStateFromRequest($this->context . '.filter.n3tworkflow.publisher', 'filter_n3tworkflow_publisher');
		$this->setState('filter.n3tworkflow.publisher', $userId);

    if (n3tWorkflowHelper::params('filter_date_created')) {
			$date = $this->getUserStateFromRequest($this->context . '.filter.n3tworkflow.created', 'filter_n3tworkflow_created', '');
	    try {
	    	new JDate($date);
				$this->setState('filter.n3tworkflow.created', $date);
	    } catch (Exception $e) {
				$this->setState('filter.n3tworkflow.created', null);
			}
		}

    if (n3tWorkflowHelper::params('filter_date_published')) {
			$date = $this->getUserStateFromRequest($this->context . '.filter.n3tworkflow.published', 'filter_n3tworkflow_published', '');
	    try {
	    	new JDate($date);
				$this->setState('filter.n3tworkflow.published', $date);
	    } catch (Exception $e) {
				$this->setState('filter.n3tworkflow.published', null);
			}
		}

		parent::populateState($ordering, $direction);
	}

	protected function getStoreId($id = '')
	{
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.category_id');
		$id .= ':' . $this->getState('filter.author_id');
    $id .= ':' . $this->getState('filter.n3tworkflow.state');
    $id .= ':' . $this->getState('filter.n3tworkflow.creator');
    $id .= ':' . $this->getState('filter.n3tworkflow.editor');
    $id .= ':' . $this->getState('filter.n3tworkflow.corrector');
    $id .= ':' . $this->getState('filter.n3tworkflow.publisher');
    if (n3tWorkflowHelper::params('filter_date_created'))
    	$id .= ':' . $this->getState('filter.n3tworkflow.created');
    if (n3tWorkflowHelper::params('filter_date_published'))
    	$id .= ':' . $this->getState('filter.n3tworkflow.published');

		return parent::getStoreId($id);
	}

	protected function getListQuery()
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$user = JFactory::getUser();

		$query->select(
			$this->getState(
				'list.select',
				'a.id, a.title, a.alias, a.checked_out, a.checked_out_time, a.catid' .
					', a.state, a.access, a.created, a.created_by, a.created_by_alias, a.modified, a.ordering, a.featured' .
					', a.publish_up, a.publish_down' .
          ', n3t.state as n3tworkflow_state' .
          ', n3t.creator as n3tworkflow_creator, un3tc.name as n3tworkflow_creator_name' .
          ', n3t.editor as n3tworkflow_editor, un3te.name as n3tworkflow_editor_name' .
          ', n3t.corrector as n3tworkflow_corrector, un3tr.name as n3tworkflow_corrector_name' .
          ', n3t.publisher as n3tworkflow_publisher, un3tp.name as n3tworkflow_publisher_name'
			)
		);
		$query->from('#__content AS a');

		$query->select('uc.name AS editor')
			->join('LEFT', '#__users AS uc ON uc.id=a.checked_out')
      ->join('LEFT', '#__n3tworkflow_content AS n3t ON n3t.article_id=a.id')
			->join('LEFT', '#__users AS un3tc ON un3tc.id=n3t.creator')
      ->join('LEFT', '#__users AS un3te ON un3te.id=n3t.editor')
      ->join('LEFT', '#__users AS un3tr ON un3tr.id=n3t.corrector')
      ->join('LEFT', '#__users AS un3tp ON un3tp.id=n3t.publisher');

		$query->select('c.title AS category_title')
			->join('LEFT', '#__categories AS c ON c.id = a.catid');

		$query->select('ua.name AS author_name')
			->join('LEFT', '#__users AS ua ON ua.id = a.created_by');

		$assogroup = 'a.id, l.title, l.image, uc.name, ag.title, c.title, ua.name';

		if (!$user->authorise('core.admin'))
		{
			$groups = implode(',', $user->getAuthorisedViewLevels());
			$query->where('a.access IN (' . $groups . ')');
			$query->where('c.access IN (' . $groups . ')');
		}

		$query->where('(a.state = 0 OR a.state = 1 AND (a.publish_up = ' . $db->quote($db->getNullDate()) . ' OR a.publish_up > ' .  $db->quote(JFactory::getDate()->toSql()) . '))');

		$baselevel = 1;
		$categoryId = $this->getState('filter.category_id');

		if (is_numeric($categoryId))
		{
			$categoryTable= JTable::getInstance('Category', 'JTable');
			$categoryTable->load($categoryId);
			$rgt = $categoryTable->rgt;
			$lft = $categoryTable->lft;
			$baselevel = (int) $categoryTable->level;
			$query->where('c.lft >= ' . (int) $lft)
				->where('c.rgt <= ' . (int) $rgt);
		}
		elseif (is_array($categoryId))
		{
			$query->where('a.catid IN (' . implode(',', ArrayHelper::toInteger($categoryId)) . ')');
		}

		$authorId = $this->getState('filter.author_id');

		if (is_numeric($authorId))
		{
			$type = $this->getState('filter.author_id.include', true) ? '= ' : '<>';
			$query->where('a.created_by ' . $type . (int) $authorId);
		}

		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			elseif (stripos($search, 'author:') === 0)
			{
				$search = $db->quote('%' . $db->escape(substr($search, 7), true) . '%');
				$query->where('(ua.name LIKE ' . $search . ' OR ua.username LIKE ' . $search . ')');
			}
			else
			{
				$search = $db->quote('%' . str_replace(' ', '%', $db->escape(trim($search), true) . '%'));
				$query->where('(a.title LIKE ' . $search . ' OR a.alias LIKE ' . $search . ')');
			}
		}

		$tagId = $this->getState('filter.tag');

		if (is_numeric($tagId))
		{
			$query->where($db->quoteName('tagmap.tag_id') . ' = ' . (int) $tagId)
				->join(
					'LEFT',
					$db->quoteName('#__contentitem_tag_map', 'tagmap')
					. ' ON ' . $db->quoteName('tagmap.content_item_id') . ' = ' . $db->quoteName('a.id')
					. ' AND ' . $db->quoteName('tagmap.type_alias') . ' = ' . $db->quote('com_content.article')
				);
		}

		$state = $this->getState('filter.n3tworkflow.state');
  	if (is_numeric($state))
			$query->where('n3t.state = ' . $state);

		$userId = $this->getState('filter.n3tworkflow.creator');
  	if (is_numeric($userId))
			$query->where('n3t.creator = ' . $userId);

		$userId = $this->getState('filter.n3tworkflow.editor');
  	if (is_numeric($userId))
			$query->where('n3t.editor = ' . $userId);

		$userId = $this->getState('filter.n3tworkflow.corrector');
  	if (is_numeric($userId))
			$query->where('n3t.corrector = ' . $userId);

		$userId = $this->getState('filter.n3tworkflow.publisher');
  	if (is_numeric($userId))
			$query->where('n3t.publisher = ' . $userId);

    if (n3tWorkflowHelper::params('filter_date_created')) {
			$date = $this->getState('filter.n3tworkflow.created');
			if (!empty($date))
	   		$query->where('DATE(a.created) = ' . $db->quote($this->getState('filter.n3tworkflow.created')));
		}

    if (n3tWorkflowHelper::params('filter_date_published')) {
			$date = $this->getState('filter.n3tworkflow.published');
			if (!empty($date))
	   		$query->where('DATE(a.publish_up) = ' . $db->quote($this->getState('filter.n3tworkflow.published')));
		}

		$query->order($db->escape('a.publish_up') . ' ' . $db->escape('DESC'));

		return $query;
	}

 public function getFilterForm($data = array(), $loadData = true)
 	{
		$form = parent::getFilterForm($data, false);

		if ($form instanceof JForm) {
    	JForm::addFormPath(__DIR__ . '/../forms');
      JForm::addFieldPath(__DIR__ . '/../fields');
			$form->loadFile('filter_n3tworkflow', $loadData);
		}

    $form = parent::getFilterForm($data, $loadData);

 		if (!n3tWorkflowHelper::params('filter_date_created'))
      $form->removeField('n3tworkflow_created', 'filter');

 		if (!n3tWorkflowHelper::params('filter_date_published'))
      $form->removeField('n3tworkflow_published', 'filter');

		return $form;
	}
}