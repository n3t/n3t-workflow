<?php
/**
 * @package n3t Workflow
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2017-2020 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

use Joomla\Registry\Registry;

class plgSystemN3tWorkflow extends JPlugin {

 	protected $autoloadLanguage = true;

	protected function overrideClass($class, $file)
	{
		jimport('joomla.filesystem.file');
		if (JFile::exists(JPATH_ROOT . $file)) {
			$content = JFile::read(JPATH_ROOT . $file);
			$content = str_replace('<?php', '', $content);
      $content = preg_replace('~class\s+' . $class . '~i', 'class n3tWorkflow' . $class, $content, 1);
      eval($content);
    	require_once (__DIR__ . '/overrides/' . $class . '.php');
		}
	}

	public function onAfterInitialise()
	{
  	JHtml::addIncludePath( __DIR__ . '/helpers/html');
		require_once(JPATH_SITE . '/plugins/system/n3tworkflow/helpers/n3tworkflow.php');

		$app = JFactory::getApplication();
		if (
				 $app->isClient('administrator')
			&& $app->input->get('option') == 'com_content'
    	&& $app->input->get('view', 'articles') == 'articles'
		) {
			$this->overrideClass('JHtmlContentAdministrator', '/administrator/components/com_content/helpers/html/contentadministrator.php');
      $this->overrideClass('ContentModelArticles', '/administrator/components/com_content/models/articles.php');
		} elseif (
				 $app->isClient('administrator')
			&& $app->input->get('option') == 'com_content'
    	&& $app->input->get('view', 'articles') == 'article'
      && $app->input->get('layout', '') == 'edit'
      && $this->params->get('edit_category', 0)
		) {
    	$this->overrideClass('JFormFieldCategoryEdit', '/administrator/components/com_categories/models/fields/categoryedit.php');
		} elseif (
				 $app->isClient('site')
			&& $app->input->get('option') == 'com_content'
    	&& $app->input->get('view', 'categories') == 'form'
      && $this->params->get('edit_category', 0)
		) {
    	$this->overrideClass('JFormFieldCategoryEdit', '/administrator/components/com_categories/models/fields/categoryedit.php');
		}
	}

 	public function onContentPrepareForm(JForm $form, $data)
	{
		$name = $form->getName();
		if ($name != 'com_content.article') return true;

		JForm::addFormPath(__DIR__ . '/forms');
    JForm::addFieldPath(__DIR__ . '/fields');
		$form->loadFile('n3tworkflow', false);

		if ($this->params->get('default_unpublished', 0))
    	$form->setFieldAttribute('state', 'default', '0');

    if ($this->params->get('hits_editable', 0)) {
    	$form->setFieldAttribute('hits', 'readonly', 'false');
      $form->setFieldAttribute('hits', 'filter', 'int');
    }

		if (!n3tWorkflowHelper::isPublisher() && !n3tWorkflowHelper::isAdmin())
    {
    	$form->removeField('publish_up');
      $form->removeField('publish_down');
    }

    return true;
	}

 	public function onContentPrepareData($context, &$data)
	{
		if (empty($data->id))
			return true;

		if ($context != 'com_content.article')
			return true;

		$data->n3tworkflow = n3tWorkflowHelper::articleWorkflow($data);

    return true;
	}

  public function onContentAfterSave($context, $item, $isNew, $data = array())
	{
		if (!is_array($data) || empty($item->id))
			return true;

		if ($context != 'com_content.article')
			return true;

		$data = isset($data['n3tworkflow']) && !empty($data['n3tworkflow']) ? $data['n3tworkflow'] : array();

    $n3tworkflow = new stdClass();
    $n3tworkflow->article_id = $item->id;
    $n3tworkflow->state = isset($data['state']) ? $data['state'] : n3tWorkflowHelper::STATE_DRAFT;
    $n3tworkflow->notes = isset($data['notes']) && !empty($data['notes']) ? $data['notes'] : null;

    $user = JFactory::getUser();
    if ($isNew)
    	$n3tworkflow->creator = $user->id;
		if (n3tWorkflowHelper::isEditor())
    	$n3tworkflow->editor = $user->id;
		if (n3tWorkflowHelper::isCorrector())
    	$n3tworkflow->corrector = $user->id;
		if (n3tWorkflowHelper::isPublisher())
    	$n3tworkflow->publisher = $user->id;

    $db = JFactory::getDbo();
		$query = $db->getQuery(true)
			->select($db->quoteName('id'))
			->from('#__n3tworkflow_content')
			->where($db->quoteName('article_id') . ' = ' . $item->id);
		$db->setQuery($query);
		$id = $db->loadResult();

		if ($id) {
      $n3tworkflow->id = $id;
      $db->updateObject('#__n3tworkflow_content', $n3tworkflow, 'id');
		} else
			$db->insertObject('#__n3tworkflow_content', $n3tworkflow);

		return true;
	}

  public function onContentAfterDelete($context, $item)
	{
		if (empty($item->id))
			return true;

		if ($context != 'com_content.article')
			return true;

    $db = JFactory::getDbo();
		$query = $db->getQuery(true)
			->delete('#__n3tworkflow_content')
			->where($db->quoteName('article_id') . ' = ' . $item->id);
		$db->setQuery($query);
    $db->execute();

    return true;
	}

  public function onAfterRoute()
  {
		$app = JFactory::getApplication();
		if ($app->isClient('administrator')) {
			if ($app->input->get('option') == 'com_content') {
	    	JHtmlSidebar::addEntry(JText::_('PLG_SYSTEM_N3TWORKFLOW_SIDEBAR_UPCOMING'), 'index.php?option=com_content&view=upcoming', $app->input->get('view', '', 'cmd') == 'upcoming');
	      if ($app->input->get('task') == 'article.edit') {
					$id = $app->input->get('id');
					if ($id) {
						$n3tworkflow = n3tWorkflowHelper::articleWorkflow($id);

		        $user = JFactory::getUser();
						$userGroups = $user->getAuthorisedGroups();

						switch ($n3tworkflow->state) {
							case n3tWorkflowHelper::STATE_EDITOR:
							case n3tWorkflowHelper::STATE_EDITOR_REJECTED:
		          	$allowed = n3tWorkflowHelper::isEditor() || n3tWorkflowHelper::isCorrector() || n3tWorkflowHelper::isPublisher() || n3tWorkflowHelper::isAdmin();
								break;
							case n3tWorkflowHelper::STATE_CORRECTOR:
							case n3tWorkflowHelper::STATE_CORRECTOR_REJECTED:
              	$allowed = n3tWorkflowHelper::isCorrector() || n3tWorkflowHelper::isPublisher() || n3tWorkflowHelper::isAdmin();
								break;
							case n3tWorkflowHelper::STATE_PUBLISHER:
							case n3tWorkflowHelper::STATE_LOCKED:
              	$allowed = n3tWorkflowHelper::isPublisher() || n3tWorkflowHelper::isAdmin();
								break;
							default:
		            $allowed = true;
								break;
						}

						if (!$allowed) {
							$app->enqueueMessage(JText::_('PLG_SYSTEM_N3TWORKFLOW_ACCESS_DENIED'), 'error');
							$app->redirect(JRoute::_('index.php?option=com_content'));
						}
					}
				} elseif ($app->input->get('view', '', 'cmd') == 'upcoming') {
	      	$base_path = JPATH_ADMINISTRATOR.'/components/com_content';
	        $controller = JControllerLegacy::getInstance('Content', array('base_path' => $base_path));
	        $controller->addViewPath(__DIR__.'/views');
	        $controller->addModelPath(__DIR__.'/models');
				}
			} elseif ($app->input->get('option') == 'com_categories' && $app->input->get('extension') == 'com_content') {
        JHtmlSidebar::addEntry(JText::_('PLG_SYSTEM_N3TWORKFLOW_SIDEBAR_UPCOMING'), 'index.php?option=com_content&view=upcoming', $app->input->get('view', '', 'cmd') == 'upcoming');
			}
		}
	}

  public function onContentBeforeDisplay($context, &$article, &$params, $limitstart)
	{
		if (!in_array($context, array('com_content.category', 'com_content.featured', 'com_content.article'))) return;

		if ($article->state != 0) return;

    $n3tworkflow = n3tWorkflowHelper::articleWorkflow($article->id);

    switch ($n3tworkflow->state) {
   		case n3tWorkflowHelper::STATE_DRAFT:
			default:
				$title = JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_WORKFLOW_STATE_NONE');
        $label = '';
				$icon = 'pencil-2';
				break;
			case n3tWorkflowHelper::STATE_AUTHOR_REJECTED:
				$title = JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_WORKFLOW_STATE_AUTHOR_REJECTED');
        $label = 'label-important';
				$icon = 'pencil-2';
				break;
			case n3tWorkflowHelper::STATE_EDITOR:
				$title = JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_WORKFLOW_STATE_EDITOR');
        $label = 'label-info';
				$icon = 'eye';
				break;
			case n3tWorkflowHelper::STATE_EDITOR_REJECTED:
				$title = JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_WORKFLOW_STATE_EDITOR_REJECTED');
        $label = 'label-important';
				$icon = 'eye';
				break;
			case n3tWorkflowHelper::STATE_CORRECTOR:
				$title = JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_WORKFLOW_STATE_CORRECTOR');
        $label = 'label-warning';
				$icon = 'checkin';
				break;
			case n3tWorkflowHelper::STATE_CORRECTOR_REJECTED:
				$title = JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_WORKFLOW_STATE_CORRECTOR_REJECTED');
        $label = 'label-warning';
				$icon = 'checkin';
				break;
			case n3tWorkflowHelper::STATE_PUBLISHER:
				$title = JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_WORKFLOW_STATE_PUBLISHER');
        $label = 'label-success';
				$icon = 'checkmark-circle';
				break;
			case 'locked':
				$title = JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_WORKFLOW_STATE_LOCKED');
        $label = 'label-inverse';
				$icon = 'lock';
				break;
		}

		return '<span class="label ' . $label . '"><i class="icon-' . $icon . '"></i> ' . $title . '</span>';
	}
}
