<?php
/**
 * @package n3t Workflow
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2017-2020 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('JPATH_PLATFORM') or die;

JFormHelper::loadFieldClass('list');

class JFormFieldN3tWorkflowState extends JFormFieldList
{
	public $type = 'n3tWorkflowState';

	protected function getOptions()
	{
    $options = [];
    $options[] = ['value' => 0, 'text' => JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_WORKFLOW_STATE_NONE')];
    $options[] = ['value' => 1, 'text' => JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_WORKFLOW_STATE_AUTHOR_REJECTED')];
    $options[] = ['value' => 10, 'text' => JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_WORKFLOW_STATE_EDITOR')];

    if (n3tWorkflowHelper::isEditor() || n3tWorkflowHelper::isCorrector() || n3tWorkflowHelper::isPublisher() || n3tWorkflowHelper::isAdmin()) {
      $options[] = ['value' => 11, 'text' => JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_WORKFLOW_STATE_EDITOR_REJECTED')];
      $options[] = ['value' => 20, 'text' => JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_WORKFLOW_STATE_CORRECTOR')];
    }

    if (n3tWorkflowHelper::isCorrector() || n3tWorkflowHelper::isPublisher() || n3tWorkflowHelper::isAdmin()) {
      $options[] = ['value' => 21, 'text' => JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_WORKFLOW_STATE_CORRECTOR_REJECTED')];
      $options[] = ['value' => 30, 'text' => JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_WORKFLOW_STATE_PUBLISHER')];
    }

    if (n3tWorkflowHelper::isPublisher() || n3tWorkflowHelper::isAdmin()) {
      $options[] = ['value' => 100, 'text' => JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_WORKFLOW_STATE_LOCKED')];
    }

		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}
