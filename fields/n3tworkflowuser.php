<?php
/**
 * @package n3t Workflow
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2017-2020 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('JPATH_PLATFORM') or die;

JFormHelper::loadFieldClass('list');

class JFormFieldN3tWorkflowUser extends JFormFieldList
{
	public $type = 'n3tWorkflowUser';

	protected function getOptions()
	{
		$group = $this->getAttribute('group', 'creator');

		switch ($group) {
			case 'creator':
			default:
				$options = n3tWorkflowHelper::getCreatorsList();
				break;
      case 'editor':
				$options = n3tWorkflowHelper::getEditorsList();
				break;
      case 'corrector':
				$options = n3tWorkflowHelper::getCorrectorsList();
				break;
      case 'publisher':
				$options = n3tWorkflowHelper::getPublishersList();
				break;
		}

		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}
