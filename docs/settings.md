Settings
========

Available settings in plugin manager are:

#### Editor usergroups

Users in these usergroups can access articles with workflow state Waiting for
Editor and Returned to Editor. If empty, access is not limited.

#### Publisher usergroups

Users in these usergroups can access articles with workflow state Waiting for
Editor, Returned to Editor, Ready to publish and Locked. If empty, access is not limited.