Release notes
=============

3.1.x
-----

#### 3.1.2

 * Limit workflow states list according to user group
 * New option to enable edit article's hits

#### 3.1.1

 * New cascade access rules
 * Creators, Editors, Correctors and Publishers filter in articles list and Upcoming list

#### 3.1.0

 * Redisegned database structure, data are stored in standalone table now
 * Numeric based workflow states
 * Creator, Editor, Corrector and Publisher user are being tracked
 * Article default state option (unpublished by default)
 * Improved installer
 * Upcoming articles view

3.0.x
-----

#### 3.0.6

 * Option to enable category change in unpublished articles, even to users without publishing right

#### 3.0.5

 * Option to filter articles by creation date or start publishing date in administration
 * Option to display start publishing date instead of creation date in administration

#### 3.0.4

 * Corrected update server address
 * Improved installer

#### 3.0.3

 * Added frontend workflow state display

#### 3.0.2

 * Added corrector role

#### 3.0.1

 * Added filtering option

#### 3.0.0

 * Initial release         