n3t Workflow
============

n3t Workflow is system plugin enabling to simple workflow process on Joomla! core
articles.

It enables to specify workflow state for an article (such as Draft, Ready to publish etc.),
displays this state in administration articles list, and enables to lock the access
to articles based on this state and selected user groups.

Moreover it enables Authors, Editors and Publishers to share notes about the article.

Installation
------------

n3t Workflow is Joomla! system plugin. It could be installed as any other extension in
Joomla!

After installing __do not forget to enable the plugin__ from Plugin Manager in your
Joomla! installation.