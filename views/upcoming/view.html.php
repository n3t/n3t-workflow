<?php
/**
 * @package n3t Workflow
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2017-2020 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

class ContentViewUpcoming extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	public $filterForm;

	public $activeFilters;

	protected $sidebar;

	public function display($tpl = null)
	{
		$this->addTemplatePath(__DIR__.'/tmpl/');
		$this->addTemplatePath(JPATH_THEMES.'/'.JFactory::getApplication()->getTemplate().'/html/com_contact/messages/');

		$this->items         = $this->get('Items');
		$this->pagination    = $this->get('Pagination');
		$this->state         = $this->get('State');
		$this->filterForm    = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');

		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

    ContentHelper::addSubmenu('');

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();

		parent::display($tpl);
	}

	protected function addToolbar()
	{
		$user	= JFactory::getUser();

		JToolbarHelper::title(JText::_('PLG_SYSTEM_N3TWORKFLOW_UPCOMING_TITLE'), 'clock article');

		if ($user->authorise('core.admin', 'com_content') || $user->authorise('core.options', 'com_content'))
		{
			JToolbarHelper::preferences('com_content');
		}

		JToolbarHelper::help('JHELP_CONTENT_ARTICLE_MANAGER');
	}

}