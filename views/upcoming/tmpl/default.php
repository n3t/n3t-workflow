<?php
/**
 * @package n3t Workflow
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2017-2020 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$app       = JFactory::getApplication();
$user      = JFactory::getUser();
$userId    = $user->get('id');
$columns   = 7;
?>

<form action="<?php echo JRoute::_('index.php?option=com_content&view=upcoming'); ?>" method="post" name="adminForm" id="adminForm">
<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif; ?>
		<?php
		// Search tools bar
		echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
		?>
		<?php if (empty($this->items)) : ?>
			<div class="alert alert-no-items">
				<?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
			</div>
		<?php else : ?>
			<table class="table table-striped" id="articleList">
				<thead>
					<tr>
						<th width="1%" class="nowrap center">
              <?php echo JText::_('JSTATUS'); ?>
						</th>
						<th style="min-width:100px" class="nowrap">
							<?php echo JText::_('JGLOBAL_TITLE'); ?>
						</th>
						<th width="10%" class="nowrap hidden-phone">
							<?php echo JText::_('JAUTHOR'); ?>
						</th>
						<th width="10%" class="nowrap hidden-phone">
							<?php echo JText::_('COM_CONTENT_HEADING_DATE_CREATED'); ?>
						</th>
						<th width="10%" class="nowrap hidden-phone">
							<?php echo JText::_('COM_CONTENT_HEADING_DATE_PUBLISH_UP'); ?>
						</th>
						<th width="1%" class="nowrap hidden-phone">
							<?php echo JText::_('JGRID_HEADING_ID'); ?>
						</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="<?php echo $columns; ?>">
						</td>
					</tr>
				</tfoot>
				<tbody>
				<?php
				$lastDate = null;
				foreach ($this->items as $i => $item) :
					$canCreate  = $user->authorise('core.create',     'com_content.category.' . $item->catid);
					$canEdit    = $user->authorise('core.edit',       'com_content.article.' . $item->id);
					$canCheckin = $user->authorise('core.manage',     'com_checkin') || $item->checked_out == $userId || $item->checked_out == 0;
					$canEditOwn = $user->authorise('core.edit.own',   'com_content.article.' . $item->id) && $item->created_by == $userId;
					$canChange  = $user->authorise('core.edit.state', 'com_content.article.' . $item->id) && $canCheckin;
					?>
					<?php
						$date = $item->publish_up > 0 ? JHtml::_('date', $item->publish_up, JText::_('DATE_FORMAT_LC3')) : JText::_('PLG_SYSTEM_N3TWORKFLOW_UPCOMING_NO_PUBLISH_UP');
						if ($date != $lastDate) {
            	$lastDate = $date;
          ?>
          	<tr class="info">
							<td colspan="<?php echo $columns; ?>">
								<strong><?php echo $date; ?></strong>
							</td>
						</tr>
          <?php
						}
					?>

					<tr class="row<?php echo $i % 2; ?>" sortable-group-id="<?php echo $item->catid; ?>">
						<td class="center">
							<div class="btn-group">
								<?php echo JHtml::_('jgrid.published', $item->state, $i, 'articles.', false, 'cb', null, null); ?>
								<?php echo JHtml::_('contentadministrator.featured', $item->featured, $i, false); ?>
                <?php echo JHtml::_('n3tworkflow.liststate', $item); ?>
							</div>
						</td>
						<td class="has-context">
							<div class="pull-left break-word">
								<?php if ($item->checked_out) : ?>
									<?php echo JHtml::_('jgrid.checkedout', $i, $item->editor, $item->checked_out_time, 'articles.', $canCheckin); ?>
								<?php endif; ?>
								<?php if ($canEdit || $canEditOwn) : ?>
									<a class="hasTooltip" href="<?php echo JRoute::_('index.php?option=com_content&task=article.edit&id=' . $item->id); ?>" title="<?php echo JText::_('JACTION_EDIT'); ?>">
										<?php echo $this->escape($item->title); ?></a>
								<?php else : ?>
									<span title="<?php echo JText::sprintf('JFIELD_ALIAS_LABEL', $this->escape($item->alias)); ?>"><?php echo $this->escape($item->title); ?></span>
								<?php endif; ?>
								<span class="small break-word">
									<?php echo JText::sprintf('JGLOBAL_LIST_ALIAS', $this->escape($item->alias)); ?>
								</span>
								<div class="small">
									<?php echo JText::_('JCATEGORY') . ': ' . $this->escape($item->category_title); ?>
								</div>
							</div>
						</td>
						<td class="small hidden-phone">
							<?php if ($item->created_by_alias) : ?>
								<a class="hasTooltip" href="<?php echo JRoute::_('index.php?option=com_users&task=user.edit&id=' . (int) $item->created_by); ?>" title="<?php echo JText::_('JAUTHOR'); ?>">
								<?php echo $this->escape($item->author_name); ?></a>
								<div class="small"><?php echo JText::sprintf('JGLOBAL_LIST_ALIAS', $this->escape($item->created_by_alias)); ?></div>
							<?php else : ?>
								<a class="hasTooltip" href="<?php echo JRoute::_('index.php?option=com_users&task=user.edit&id=' . (int) $item->created_by); ?>" title="<?php echo JText::_('JAUTHOR'); ?>">
								<?php echo $this->escape($item->author_name); ?></a>
							<?php endif; ?>
              <?php
								$users = array();
                if ($item->n3tworkflow_creator)
									$users[] = JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_CREATOR') . ': <a href="' . JRoute::_('index.php?option=com_users&task=user.edit&id=' . (int) $item->n3tworkflow_creator) . '">' . $this->escape($item->n3tworkflow_creator_name) .'</a>';
                if ($item->n3tworkflow_editor)
									$users[] = JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_EDITOR') . ': <a href="' . JRoute::_('index.php?option=com_users&task=user.edit&id=' . (int) $item->n3tworkflow_editor) . '">' . $this->escape($item->n3tworkflow_editor_name) .'</a>';
                if ($item->n3tworkflow_corrector)
									$users[] = JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_CORRECTOR') . ': <a href="' . JRoute::_('index.php?option=com_users&task=user.edit&id=' . (int) $item->n3tworkflow_corrector) . '">' . $this->escape($item->n3tworkflow_corrector_name) .'</a>';
                if ($item->n3tworkflow_publisher)
									$users[] = JText::_('PLG_SYSTEM_N3TWORKFLOW_FIELD_PUBLISHER') . ': <a href="' . JRoute::_('index.php?option=com_users&task=user.edit&id=' . (int) $item->n3tworkflow_publisher) . '">' . $this->escape($item->n3tworkflow_publisher_name) .'</a>';

								if ($users) {
							?>
	              <div class="small muted"><?php echo implode(', ', $users); ?></div>
              <?php } ?>
						</td>
						<td class="nowrap small hidden-phone">
							<?php
							$date = $item->created;
							echo $date > 0 ? JHtml::_('date', $date, JText::_('DATE_FORMAT_LC4')) : '-';
							?>
						</td>
						<td class="nowrap small hidden-phone">
							<?php
							$date = $item->publish_up;
							echo $date > 0 ? JHtml::_('date', $date, JText::_('DATE_FORMAT_LC2')) : '-';
							?>
						</td>
						<td class="hidden-phone">
							<?php echo (int) $item->id; ?>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php endif; ?>

		<?php echo $this->pagination->getListFooter(); ?>

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
